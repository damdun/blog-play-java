package models;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

public class Article {

    private Long id;
    @NotBlank
    @Length(min = 5)
    private String title;
    @NotBlank
    @Length(min = 10)
    private String text;

    public Article() {
        this(null, null, null);
    }

    public Article(String title, String text) {
        this(null, title, text);
    }

    public Article(Long id, String title, String text) {
        this.id = id;
        this.title = title;
        this.text = text;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
