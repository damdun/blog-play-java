package controllers;

import models.Article;
import play.data.Form;
import play.db.DB;
import play.mvc.Controller;
import play.mvc.Result;
import services.ArticlesDatabase;
import views.html.create;
import views.html.edit;
import views.html.index;
import views.html.show;

public class Application extends Controller {

    private ArticlesDatabase database = new ArticlesDatabase(DB.getConnection());

    private Form<Article> form = Form.form(Article.class);

    public Result showList() {
        return ok(index.render(database.getArticles()));
    }

    public Result initCreate() {
        return ok(create.render(new Article()));
    }

    public Result doCreate() {
        Form<Article> boundForm = form.bindFromRequest();
        if (boundForm.hasErrors()) {
            System.out.println(boundForm.errors());

            return badRequest(create.render(new Article()));
        } else {
            database.insertArticle(boundForm.get());
            return redirect(routes.Application.showList());
        }
    }

    public Result showDetails(Long id) {
        return ok(show.render(database.findArticle(id)));
    }

    public Result initEdit(Long id) {
        return ok(edit.render(database.findArticle(id)));
    }

    public Result doEdit(Long id) {
        Form<Article> boundForm = form.bindFromRequest();
        if (boundForm.hasErrors()) {
            System.out.println(boundForm.errors());

            return badRequest(edit.render(database.findArticle(id)));
        } else {
            database.updateArticle(id, boundForm.get());
            return redirect(routes.Application.showList());
        }
    }

    public Result doDelete(Long id) {
        return ok();
    }

}
